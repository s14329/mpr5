package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Comparator;

public class OrdersService {
    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> orders) {
        List<Order> ordersMoreThan5streams = orders.stream()
                .filter(order -> order.getItems().size() > 5)
                .collect(Collectors.toList());

        return ordersMoreThan5streams;
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> orders) {
        ClientDetails oldestClient = orders.stream()
                .map(order -> order.getClientDetails())
                .max(Comparator.comparing(ClientDetails::getAge))
                .get();

        return oldestClient;
    }

    public static Order findOrderWithLongestComments(List<Order> orders) {
        Order orderWithLongestComments = orders.stream()
                .max((x,y) -> x.getComments().length() - y.getComments().length())
                .get();

        return orderWithLongestComments;
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> orders) {
        String namesAbove18Years = orders.stream()
                .filter(order -> order.getClientDetails().getAge() > 17)
                .map(order -> order.getClientDetails().getName() + " " + order.getClientDetails().getSurname())
                .distinct()
                .collect(Collectors.joining(", "));

        return namesAbove18Years;
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> orders) {
        List<String> ordersItemsNamesfromOrderWithCommentA = orders.stream()
                .filter(order -> order.getComments().startsWith("A"))
                .flatMap(order -> order.getItems().stream())
                .map(item -> item.getName())
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        return ordersItemsNamesfromOrderWithCommentA;
    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> orders) {
        orders.stream()
                .filter(order -> order.getClientDetails().getName().startsWith("S"))
                .map(order -> order.getClientDetails().getLogin().toUpperCase())
                .forEach(System.out::println);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> orders) {
        Map<ClientDetails, List<Order>> ordersByClient = orders.stream()
                .collect(Collectors.groupingBy(Order::getClientDetails));

        return ordersByClient;
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> orders) {
        Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = orders.stream()
                .map(order -> order.getClientDetails())
                .distinct()
                .collect(Collectors.partitioningBy(clientDetails -> clientDetails.getAge() > 17));

        return clientsUnderAndOver18;
    }
}
